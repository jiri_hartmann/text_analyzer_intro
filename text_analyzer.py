'''
author = Jiri Hartmann
'''
import re
TEXTS = ['''
Situated about 10 miles west of Kemmerer, 
Fossil Butte is a ruggedly impressive 
topographic feature that rises sharply 
some 1000 feet above Twin Creek Valley 
to an elevation of more than 7500 feet 
above sea level. The butte is located just 
north of US 30N and the Union Pacific Railroad, 
which traverse the valley. ''',

         '''At the base of Fossil Butte are the bright 
red, purple, yellow and gray beds of the Wasatch 
Formation. Eroded portions of these horizontal 
beds slope gradually upward from the valley floor 
and steepen abruptly. Overlying them and extending 
to the top of the butte are the much steeper 
buff-to-white beds of the Green River Formation, 
which are about 300 feet thick.''',

         '''The monument contains 8198 acres and protects 
a portion of the largest deposit of freshwater fish 
fossils in the world. The richest fossil fish deposits 
are found in multiple limestone layers, which lie some 
100 feet below the top of the butte. The fossils 
represent several varieties of perch, as well as 
other freshwater genera and herring similar to those 
in modern oceans. Other fish such as paddlefish, 
garpike and stingray are also present.'''
         ]

users = [('bob', '123'), ('ann', 'pass123'), ('mike', 'password123'), ('liz', 'pass123')]

print('-'*40)
print('Welcome to the app. Please log in:')
username = input('USERNAME: ')
password = input('PASSWORD: ')
print('-'*40)

allowentry = False
for user in users:
    if (user[0] == username) and (user[1] == password):
        allowentry = True
        break
if allowentry == False:
    print("Name or password is not correct.")
    exit(0)

print("We have 3 texts to be analyzed.")
numberoftext = input("Enter a number btw. 1 and 3 to select: ")
numberoftext = int(numberoftext) - 1  # transposition to index
if not numberoftext in range(len(TEXTS)):
    print("Number is out of range.")
    exit(0)

textinwords = re.split('[\W+\s+]', TEXTS[numberoftext])
while "" in textinwords:
    textinwords.remove("")

word_all = len(textinwords)
word_title = 0
word_upper = 0
word_lower = 0
word_numeric = 0
word_numeric_value = 0
maxwordlength = 0

for word in textinwords:
    if word.istitle():
        word_title += 1
    if word.isupper():
        word_upper += 1
    if word.islower():
        word_lower += 1
    if word.isnumeric():
        word_numeric += 1
        word_numeric_value += float(word)
    if len(word) > maxwordlength:
        maxwordlength = len(word)

bars = list(range(maxwordlength+1))
for i in range(maxwordlength+1):
    bars[i] = 0
for word in textinwords:
    bars[len(word)] += 1

print('-'*40)
for i in range(1, maxwordlength+1):
    if bars[i] != 0:
        print(str(i) + " " + bars[i]*"*" + " " + str(bars[i]))

print('-'*40)
print(f'There are {word_all} words in the selected text.')
print(f'There are {word_title} titlecase words.')
print(f'There are {word_upper} uppercase words.')
print(f'There are {word_lower} lowercase words.')
print(f'There are {word_numeric} numeric strings.')
print('-'*40)
print(f'If we summed all the numbers in this text we would get: {word_numeric_value}')
print('-'*40)

